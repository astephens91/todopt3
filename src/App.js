import React, { Component } from "react";
import "./index.css";
import TodoList from "./components/TodoList";
import { Route, Switch, NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  addTodo,
  toggleTodo,
  deleteTodo,
  clearCompletedTodo
} from "./actions/actions";
import storeInstance from "./index";

class App extends Component {
  state = {
    input: ""
  };

  handleChange = event => {
    this.setState({ input: event.target.value });
  };

  addToDo = event => {
    if (event.keyCode === 13) {
      const newTodo = {
        id: Math.floor(Math.random() * 19847530),
        userID: 1,
        title: this.state.input,
        completed: false
      };
      storeInstance.dispatch(addTodo(newTodo));
      event.target.value = "";
    }
  };

  clearCompleted = event => {
    storeInstance.dispatch(clearCompletedTodo());
  };

  toggleComplete = todo => id => {
    storeInstance.dispatch(toggleTodo(todo));
  };

  deleteOne = id => event => {
    storeInstance.dispatch(deleteTodo(id));
  };

  render() {
    let counter = 0;
    for(let i=0; i<this.props.todos.length; i++){
      if(this.props.todos[i].completed === false){
        counter++
      }
    }
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autoFocus
            onChange={this.handleChange}
            value={this.state.value}
            onKeyDown={this.addToDo}
          />
        </header>
        <Switch>
          <Route
            exact
            path="/active"
            render={props => (
              <React.Fragment>
                <TodoList
                  {...props}
                  filter="active"
                  todos={this.state.todos}
                  toggleComplete={this.toggleComplete}
                  deleteOnee={this.deleteOne}
                  handleFilters={this.handleFilters}
                />
              </React.Fragment>
            )}
          />
          <Route
            exact
            path="/completed"
            render={props => (
              <React.Fragment>
                <TodoList
                  {...props}
                  filter="completed"
                  todos={this.state.todos}
                  toggleComplete={this.toggleComplete}
                  deleteOne={this.deleteOne}
                  handleFilters={this.handleFilters}
                />
              </React.Fragment>
            )}
          />
          <Route
            path="/"
            render={props => (
              <React.Fragment>
                <TodoList
                  {...props}
                  filter="all"
                  todos={this.state.todos}
                  toggleComplete={this.toggleComplete}
                  handleDelete={this.handleDelete}
                />
              </React.Fragment>
            )}
          />
        </Switch>
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>{counter}</strong> item(s) left
          </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">
                All
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">
                Active
              </NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">
                Completed
              </NavLink>
            </li>
          </ul>
          <button
            className="clear-completed"
            onClick={this.clearCompleted}
          >
            Clear completed
          </button>
        </footer>
      </section>
    );
  }
}


const mapStateToProps = state => {
  return {
    todos: state.todos,
    value: state.value
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToDo: bindActionCreators(addTodo, dispatch),
    toggleTodo: bindActionCreators(toggleTodo, dispatch),
    deleteTodo: bindActionCreators(deleteTodo, dispatch),
    clearCompletedTodo: bindActionCreators(clearCompletedTodo, dispatch)
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
