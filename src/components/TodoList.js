import React, { Component } from "react";
import TodoItem from "./TodoItem";
import { connect } from "react-redux";

class TodoList extends Component {
  render() {
    let filteredItems;
    switch (this.props.filter) {
      case "active":
        filteredItems = this.props.todos.filter(todo => !todo.completed);
        break;
      case "completed":
        filteredItems = this.props.todos.filter(todo => todo.completed);
        break;
      default:
        filteredItems = this.props.todos;
    }
    return (
      <section className="main">
        <ul className="todo-list">
          {filteredItems.map(todo => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              todos={this.props.todos}
              
              onChange={() => this.props.onChange(todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

function mapStateToProps(state) {
  return {
    todos: state.todos,
    value: state.value
  };
}

export default connect(
  mapStateToProps,
  null
)(TodoList);
