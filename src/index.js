import React from "react";
import { render } from "react-dom";
import { todosReducer } from "./reducers/reducers.js";
import { createStore } from "redux";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import "./index.css";
import App from "./App";
let storeInstance = createStore(todosReducer);

render(
  <BrowserRouter>
    <Provider store={storeInstance}>
      <App />
    </Provider>
  </BrowserRouter>,
  document.getElementById("root")
);

export default storeInstance;

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
