import {
  ADD_TODO,
  TOGGLE_TODO,
  DELETE_TODO,
  CLEAR_COMPLETED_TODOS
} from "../actions/actions";
import todosList from "../todos.json";

const defaultState = {
  todos: todosList
};

export const todosReducer = (state = defaultState, action) => {
  switch (action.type) {
    case ADD_TODO:
      const newList = [...state.todos, action.payload];
      return Object.assign({}, state, { todos: newList });
    case TOGGLE_TODO:
      const toggleList = state.todos.map(todo =>
        todo.id === action.payload
          ? { ...todo, completed: !todo.completed }
          : todo
      );
      return Object.assign({}, state, { todos: toggleList });
    case DELETE_TODO:
      const toDeleteList = state.todos.filter(
        todo => todo.id !== action.payload
      );
      return Object.assign({}, state, { todos: toDeleteList });
    case CLEAR_COMPLETED_TODOS:
      const notCompletedTodosList = state.todos.filter(
        todo => todo.completed === false
      );
      return Object.assign({}, state, { todos: notCompletedTodosList });

    default:
      return state;
  }
};
